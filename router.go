// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooterdatadog

import "net/http"

import (
	muxtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorilla/mux"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
	"gitlab.com/donesky.io/rooter"
	"github.com/gorilla/mux"
)

type dataDogRouter struct {
	router *muxtrace.Router
}

func StartDataDogTracer() {
	tracer.Start()
}

func NewDataDogRouter(serviceName string) rooter.RouteManagerRouter {
	return &dataDogRouter{
		router: muxtrace.NewRouter(muxtrace.WithServiceName(serviceName)),
	}
}

func (r *dataDogRouter) PathPrefix(path string) *mux.Route {
	return r.router.PathPrefix(path)
}

func (r *dataDogRouter) HandleFunc(path string, handler func(w http.ResponseWriter, r *http.Request)) *mux.Route {
	return r.router.HandleFunc(path, handler)
}

func (r *dataDogRouter) SetNotFoundHandler(handler http.Handler) {
	r.router.NotFoundHandler = handler
}

func (r *dataDogRouter) ServeHTTP(w http.ResponseWriter, request *http.Request) {
	r.router.ServeHTTP(w, request)
}
